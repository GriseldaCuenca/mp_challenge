//
//  Payment.swift
//  challengemp
//
//  Created by Fernando Elejanagoitia on 20/08/2018.
//  Copyright © 2018 Griselda Cuenca. All rights reserved.
//

import Foundation

class Payment {
    var amount : String
    var card : String
    var cardDescription : String
    var bank : String
    var bankDescription : String
    var isPaymentComplete : Bool
    // Cuota
    var instalment : String
    
    private init() {
        self.amount = ""
        self.card = ""
        self.cardDescription = ""
        self.bank = ""
        self.bankDescription = ""
        self.instalment = ""
        self.isPaymentComplete = false
    }
    
    static let sharedInstance = Payment()
}
