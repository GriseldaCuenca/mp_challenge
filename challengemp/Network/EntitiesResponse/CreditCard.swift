//
//  CreditCard.swift
//  challengemp
//
//  Created by Fernando Elejanagoitia on 18/08/2018.
//  Copyright © 2018 Griselda Cuenca. All rights reserved.
//

import Foundation

struct CreditCard: Codable {
    let id: String
    let name: String
    let payment_type_id: String
    let thumbnail: URL
    let secure_thumbnail: URL
}
