//
//  Fee.swift
//  challengemp
//
//  Created by Fernando Elejanagoitia on 18/08/2018.
//  Copyright © 2018 Griselda Cuenca. All rights reserved.
//

import Foundation

struct Fee: Codable {
    let payment_method_id: String
    let payment_type_id: String
    let payer_costs: [Cost]
}
