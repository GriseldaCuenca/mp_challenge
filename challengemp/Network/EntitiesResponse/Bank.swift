//
//  Bank.swift
//  challengemp
//
//  Created by Fernando Elejanagoitia on 18/08/2018.
//  Copyright © 2018 Griselda Cuenca. All rights reserved.
//

import Foundation

struct Bank: Codable {
    let id: String
    let name: String
    let secure_thumbnail: URL
    let thumbnail: URL
}
