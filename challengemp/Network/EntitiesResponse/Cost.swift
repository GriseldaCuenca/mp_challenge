//
//  Cost.swift
//  challengemp
//
//  Created by Fernando Elejanagoitia on 18/08/2018.
//  Copyright © 2018 Griselda Cuenca. All rights reserved.
//

import Foundation

struct Cost: Codable {
    let installments: Int
    let recommended_message: String
}
