//
//  APIRouter.swift
//  challengemp
//
//  Created by Fernando Elejanagoitia on 20/08/2018.
//  Copyright © 2018 Griselda Cuenca. All rights reserved.
//

import Foundation
import Alamofire

enum APIRouter: URLRequestConvertible {
    
    // Lista de tarjetas de débito y crédito
    case getPaymentMethod()
    // Lista de bancos según tarjeta seleccionada
    case getBank(payment_method_id: String)
    // Lista de cuotas según tarjeta y banco
    case getInstallments(amount: String, payment_method_id: String, issuerId: String)
    
    // MARK: - HTTPMethod
    private var method: HTTPMethod {
        switch self {
        case .getPaymentMethod, .getBank, .getInstallments:
            return .get
        }
    }
    
    // MARK: - Path
    private var path: String {
        switch self {
        case .getPaymentMethod:
            return ""
        case .getBank:
            return "/card_issuers"
        case .getInstallments:
            return "/installments"
        }
    }
    
    // MARK: - Parameters
    private var parameters: [String: Any]? {
        switch self {
        case .getPaymentMethod:
            return [K.APIParameterKey.public_key: K.ProductionServer.publicKey]
        case .getBank(let payment_method_id):
            return [K.APIParameterKey.public_key: K.ProductionServer.publicKey, K.APIParameterKey.payment_method_id: payment_method_id]
        case .getInstallments(let amount, let paymentMethodId, let issuerId):
            return [K.APIParameterKey.public_key: K.ProductionServer.publicKey, K.APIParameterKey.amount: amount, K.APIParameterKey.payment_method_id: paymentMethodId, K.APIParameterKey.issuerid: issuerId]
        }
    }
    
    // MARK: - URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        let url = try K.ProductionServer.baseURL.asURL()
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        
        // HTTP Method
        urlRequest.httpMethod = method.rawValue
        
        //return urlRequest
        return try URLEncoding.default.encode(urlRequest, with: parameters)
    }
    
}
