//
//  Constants.swift
//  challengemp
//
//  Created by Fernando Elejanagoitia on 20/08/2018.
//  Copyright © 2018 Griselda Cuenca. All rights reserved.
//

import Foundation

struct K {
    struct ProductionServer {
        static let baseURL = "https://api.mercadopago.com/v1/payment_methods"
        static let publicKey = "444a9ef5-8a6b-429f-abdf-587639155d88"
    }
    
    struct APIParameterKey {
        static let public_key = "public_key"
        static let payment_method_id = "payment_method_id"
        static let amount = "amount"
        static let issuerid = "issuer.id"
    }
    
}

enum HTTPHeaderField: String {
    case authentication = "Authorization"
    case contentType = "Content-Type"
    case acceptType = "Accept"
    case acceptEncoding = "Accept-Encoding"
}

enum ContentType: String {
    case json = "application/json"
}
