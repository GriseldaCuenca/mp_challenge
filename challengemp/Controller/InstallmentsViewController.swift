//
//  InstallmentsViewController.swift
//  challengemp
//
//  Created by Fernando Elejanagoitia on 21/08/2018.
//  Copyright © 2018 Griselda Cuenca. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class InstallmentsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    var installmentsList = [Fee]()
    var payerCostList = [Cost]()
    
    @IBOutlet weak var installmentsTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        /*
        Alamofire.request(K.ProductionServer.baseURL + "/installments",
                          method: .get,
                          parameters: [
                            "public_key": K.ProductionServer.publicKey,
                            "payment_method_id" : Payment.sharedInstance.card,
                            "amount" : Payment.sharedInstance.amount,
                            "issuer.id" : Payment.sharedInstance.bank
            ]).responseJSON { response in
                let json = response.data
                
                do{
                    //created the json decoder
                    let decoder = JSONDecoder()
                    
                    //using the array to put values
                    self.installmentsList = try decoder.decode([Fee].self, from: json!)
                    
                    for feeItem in self.installmentsList{
                        for payer_cost in feeItem.payer_costs {
                            self.payerCostList.append(payer_cost)
                        }
                    }
                    
                    
                }catch let err{
                    print(err)
                }
                self.installmentsTableView.reloadData()
        }*/
        
        Alamofire.request(APIRouter.getInstallments(amount: Payment.sharedInstance.amount, payment_method_id: Payment.sharedInstance.card, issuerId: Payment.sharedInstance.bank)).responseJSON { response in
                let json = response.data
                
                do{
                    //created the json decoder
                    let decoder = JSONDecoder()
                    
                    //using the array to put values
                    self.installmentsList = try decoder.decode([Fee].self, from: json!)
                    
                    for feeItem in self.installmentsList{
                        for payer_cost in feeItem.payer_costs {
                            self.payerCostList.append(payer_cost)
                        }
                    }
                    
                    
                }catch let err{
                    print(err)
                }
                self.installmentsTableView.reloadData()
        }
        self.installmentsTableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return payerCostList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! InstallmentsTableViewCell
        
        let installments: Cost
        installments = payerCostList[indexPath.row]
        
        cell.textInstallment.text = installments.recommended_message
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print ("You selected row \(indexPath.row)")
        print ("You selected row id is \(payerCostList[indexPath.row].installments)")
        Payment.sharedInstance.instalment = payerCostList[indexPath.row].recommended_message
        Payment.sharedInstance.isPaymentComplete = true
        self.performSegue(withIdentifier: "unwindToAmountViewController", sender: self)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
