//
//  AmountViewController.swift
//  challengemp
//
//  Created by Fernando Elejanagoitia on 21/08/2018.
//  Copyright © 2018 Griselda Cuenca. All rights reserved.
//

import UIKit

class AmountViewController: UIViewController {

    @IBOutlet weak var inputAmount: UITextField!
    @IBOutlet weak var btnContinue: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func viewDidAppear() {
        if (Payment.sharedInstance.isPaymentComplete) {
            self.showAlert("Valores ingresado", messageToShow: "Por favor, ingrese usuario y contraseña.")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func enterAmount(_ sender: Any) {
        Payment.sharedInstance.amount = inputAmount.text!
        self.performSegue(withIdentifier: "goToCardList", sender: self)
    }
    
    @IBAction func prepareForUnwind(segue: UIStoryboardSegue) {
        DispatchQueue.main.async {
            if (Payment.sharedInstance.isPaymentComplete) {
                self.showAlert("Valores ingresado", messageToShow: "Monto: \(Payment.sharedInstance.amount) \n Tarjeta: \(Payment.sharedInstance.cardDescription) \n Banco: \(Payment.sharedInstance.bankDescription) \n Cuotas: \(Payment.sharedInstance.instalment)")
            }
        }
    }
    
    func showAlert(_ title: String, messageToShow: String) -> Void {
        let alert = UIAlertController(title:title, message:messageToShow, preferredStyle: .alert) // 1
        let firstAction = UIAlertAction(title: "Aceptar", style: .default) { (alert: UIAlertAction!) -> Void in
        }
        alert.addAction(firstAction)
        present(alert, animated: true, completion:nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
