//
//  InstallmentsTableViewCell.swift
//  challengemp
//
//  Created by Fernando Elejanagoitia on 21/08/2018.
//  Copyright © 2018 Griselda Cuenca. All rights reserved.
//

import UIKit

class InstallmentsTableViewCell: UITableViewCell {

    @IBOutlet weak var textInstallment: UILabel!
    
   override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
