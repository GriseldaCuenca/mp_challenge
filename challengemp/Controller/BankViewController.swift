//
//  BankViewController.swift
//  challengemp
//
//  Created by Fernando Elejanagoitia on 21/08/2018.
//  Copyright © 2018 Griselda Cuenca. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class BankViewController: UIViewController, UITableViewDataSource, UITableViewDelegate  {
    
    var bankList = [Bank]()
    
    @IBOutlet weak var bankTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        /*
        Alamofire.request(K.ProductionServer.baseURL + "/card_issuers",
                          method: .get,
                          parameters: [
                            "public_key": K.ProductionServer.publicKey,
                            "payment_method_id" : Payment.sharedInstance.card
            ]).responseJSON { response in
                let json = response.data
                
                do{
                    //created the json decoder
                    let decoder = JSONDecoder()
                    
                    //using the array to put values
                    self.bankList = try decoder.decode([Bank].self, from: json!)
            
                    
                }catch let err{
                    print(err)
                }
                self.bankTableView.reloadData()
        }*/
        
        // Mejora
        Alamofire.request(APIRouter.getBank(payment_method_id: Payment.sharedInstance.card)).responseJSON { response in
                let json = response.data
                
                do{
                    //created the json decoder
                    let decoder = JSONDecoder()
                    
                    //using the array to put values
                    self.bankList = try decoder.decode([Bank].self, from: json!)
                    
                }catch let err{
                    print(err)
                }
                self.bankTableView.reloadData()
        }
        self.bankTableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return bankList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! BankTableViewCell
        
        let bank: Bank
        bank = bankList[indexPath.row]
        
        cell.textBank.text = bank.name
        
        //displaying image
        Alamofire.request(bank.secure_thumbnail).responseImage { response in
            debugPrint(response)
            
            if let image = response.result.value {
                cell.imgBank.image = image
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print ("You selected row \(indexPath.row)")
        print ("You selected row id is \(bankList[indexPath.row].id)")
        Payment.sharedInstance.bank = bankList[indexPath.row].id
        Payment.sharedInstance.bankDescription = bankList[indexPath.row].name
        self.performSegue(withIdentifier: "goToInstallmentsList", sender: self)
    }

}
