//
//  ViewController.swift
//  challengemp
//
//  Created by Fernando Elejanagoitia on 18/08/2018.
//  Copyright © 2018 Griselda Cuenca. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    var cardList = [CreditCard]()
    var creditCardList = [CreditCard]()

    @IBOutlet weak var tableViewCards: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        /*
        Alamofire.request(K.ProductionServer.baseURL,
                          method: .get,
                          parameters: [
                            "public_key": K.ProductionServer.publicKey
            ]).responseJSON { response in
            let json = response.data
            
            do{
                //created the json decoder
                let decoder = JSONDecoder()
                
                //using the array to put values
                self.cardList = try decoder.decode([CreditCard].self, from: json!)
                
                for cardItem in self.cardList{
                    if (cardItem.payment_type_id == "credit_card") {
                        self.creditCardList.append(cardItem)
                    }
                }
                print (self.creditCardList.count)
                
            }catch let err{
                print(err)
            }
            self.tableViewCards.reloadData()
        }
 */
        Alamofire.request(APIRouter.getPaymentMethod()).responseJSON { response in
                let json = response.data
                
                do{
                    //created the json decoder
                    let decoder = JSONDecoder()
                    
                    //using the array to put values
                    self.cardList = try decoder.decode([CreditCard].self, from: json!)
                    
                    for cardItem in self.cardList{
                        if (cardItem.payment_type_id == "credit_card") {
                            self.creditCardList.append(cardItem)
                        }
                    }
                    print (self.creditCardList.count)
                    
                }catch let err{
                    print(err)
                }
                self.tableViewCards.reloadData()
        }
        self.tableViewCards.reloadData()
 
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return creditCardList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CardTableViewCell
        
        let card: CreditCard
        card = creditCardList[indexPath.row]
        
        cell.textCard.text = card.name
        
        //displaying image
        Alamofire.request(card.secure_thumbnail).responseImage { response in
            debugPrint(response)
            
            if let image = response.result.value {
                cell.imgCard.image = image
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print ("You selected row \(indexPath.row)")
        print ("You selected row id is \(creditCardList[indexPath.row].id)")
        Payment.sharedInstance.card = creditCardList[indexPath.row].id
        Payment.sharedInstance.cardDescription = creditCardList[indexPath.row].name
        self.performSegue(withIdentifier: "goToBankList", sender: self)
    }


}

